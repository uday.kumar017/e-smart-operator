Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  match "/eta_demo", :to => "customers#eta_demo", via: :get
end
