source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.1'

# Use sqlite3 as the database for Active Record
# gem 'sqlite3'

# Use mysql2 as the database for Active Record
gem 'mysql2', '~> 0.4.6'

# Use Puma as the app server
gem 'puma', '~> 3.9', '>= 3.9.1'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.3', '>= 3.3.3'
gem 'redis-rails', '~> 5.0', '>= 5.0.2' # https://github.com/redis-store/redis-rails

gem 'daemons', '~> 1.2', '>= 1.2.6'
# gem 'site24x7_apminsight', '~> 1.6', '>= 1.6.1'
gem 'site24x7_apminsight', '~> 1.5', '>= 1.5.3'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Exception Tracking!
gem 'honeybadger', '~> 3.1', '>= 3.1.2'

gem 'rack-cors', '~> 0.4.1', :require => 'rack/cors' # https://github.com/cyu/rack-cors
gem 'rack-attack', '~> 5.0', '>= 5.0.1' # https://github.com/kickstarter/rack-attack

# Sidekiq Gem for BG Jobs Processing
gem 'sidekiq', '~> 5.0', '>= 5.0.3' #
gem 'sidekiq-failures' # https://github.com/mhfs/sidekiq-failures/
gem 'sidekiq-unique-jobs' # https://github.com/mhenrixon/sidekiq-unique-jobs
gem 'sidekiq-scheduler', '~> 2.1', '>= 2.1.5' # https://rubygems.org/gems/sidekiq-scheduler
gem 'redis-namespace'

# SuckerPunch for Instant Asynchronous Jobs
gem 'sucker_punch', '~> 2.0', '>= 2.0.2'

# Use Capistrano for deployment
gem 'capistrano-rails', group: :development

# New Gems Added !
#gem 'activerecord-userstamp', :git => "https://github.com/lowjoel/activerecord-userstamp.git", :branch => "master"
# New Gem Added
# gem 'audited', '~> 4.5'
# gem "audited", github: "collectiveidea/audited"
#gem "audited",  "4.5.0", :path =>"vendor/gems/audited-4.5.0"
gem 'devise', '~> 4.3'
gem 'kaminari'
gem 'will_paginate'
gem 'will_paginate-bootstrap'
gem "breadcrumbs_on_rails"
#gem "simplehttp", "0.1.3", :path => "vendor/gems/simplehttp-0.1.3"
gem "xml-simple"
gem "jquery-rails"
gem 'countries', :require => 'countries/global'
gem 'google_url_shortener'
# maps gems
gem 'underscore-rails'
gem 'geocoder'
gem 'gmaps4rails'
gem 'cities', '~> 0.3.1'
gem "nested_form"
gem 'wkhtmltopdf-binary'
gem 'wicked_pdf'
gem 'humanize'
# gem 'scout_apm'

# group :development, :test do
#   # Call 'byebug' anywhere in the code to stop execution and get a debugger console
#   gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
#   # Adds support for Capybara system testing and selenium driver
#   gem 'capybara', '~> 2.13'
#   gem 'selenium-webdriver'
# end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'pry-rails'
  gem "pry-nav"

  gem 'pronto', require: false
  gem 'pronto-brakeman', require: false
  gem 'pronto-flay', require: false
  gem 'pronto-poper', require: false
  gem 'pronto-rails_best_practices', require: false
  gem 'pronto-reek', require: false
  gem 'pronto-rubocop', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'jquery-datatables-rails'
gem 'ajax-datatables-rails'
gem 'write_xlsx'
gem 'mail','~> 2.7.0'
# For LogDNA
gem "logdna-rails"
gem "lograge"
gem 'logstash-event'
