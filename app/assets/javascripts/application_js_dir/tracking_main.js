// Email id validation function.
function validateEmail(email) { 
    // var re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   // /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhoneNumber(phone_no) { 
    var re = /^(8|9|7)[0-9]{9}$/;
     return re.test(phone_no);
}

function getAllIds(class_name) {
  temp = $('.' + class_name);
  ids = {};
     for ( i = 0; i < temp.length; i++) {
      if (temp[i].checked == true) {
      	  ids[temp[i]["id"]] = $(".cls_"+class_name+"_"+temp[i]["id"]).val();
	  }
	}
  return ids;
}

function selectAllMapping(class_name) {
	 $("#select_all").change(function(){  //"select all" change 
    var status = this.checked; // "select all" checked status
    $('.' + class_name).each(function(){ //iterate all listed checkbox items
        this.checked = status; //change ".checkbox" checked status
    });
});
}
function updateAllMapping(controller_name, action_name, class_name){
	var data_hash = getAllIds(class_name);
	if(jQuery.isEmptyObject(data_hash)){
		alert("please select a record!")
		return false;
	}
	  url_path = "/"+ controller_name +"/" + action_name
	  $.ajax({
	    url : url_path,
	    data: {
	      data_hash: data_hash
	    },
	    success : function(result) {

	   }
	})
}

$(function() {
	$(".date_picker").datepicker({ 
	    format: 'yyyy-mm-dd',
	});
});
 $(function() {
    $(".datetimepicker").datetimepicker({
      pickDate: false,
      format: "hh:mm", 
      pick24HourFormat: true,
      pickSeconds: false,
    });
  });

  function filterDashbordData(controller_name, action_name, status){
    if(status == "pie_chart"){
      var asset_id = $("#asset_id").val();
      var from_date = $("#pie_chart_from_date").val();
      var to_date = $("#pie_chart_to_date").val();
      url_path = "/"+ controller_name +"/" + action_name +"?asset_id=" + asset_id + "&from_date=" + from_date + "&to_date=" + to_date +"&status=" + status
    }else{
      var service_id = $("#service_id").val();
      var from_date = $("#from_date").val();
      var to_date = $("#to_date").val();
      url_path = "/"+ controller_name +"/" + action_name +"?service_id=" + service_id + "&from_date=" + from_date + "&to_date=" + to_date +"&status=" + status
    }
    $.ajax({
      url : url_path,
      success : function(result) {

     }
  })
 }
 
 function loadStatesBasedOnCountries(id_value, list_name){
  url_path = "/accounts/load_states_based_on_countries?"+list_name+"="+id_value;
  $.ajax({
      url: url_path,
      success: function(result) {

     }
  })
 }

function loadStatesBasedOnCountriesForBranch(id_value, list_name){
  url_path = "/branches/load_states_based_on_countries?"+list_name+"="+id_value;
  $.ajax({
      url: url_path,
      success: function(result) {

     }
  })
 }

 function load_states_based_on_countries(id_value, list_name){
  url_path = "/sys_destinations/load_states_based_on_countries?"+list_name+"="+id_value;
  $.ajax({
      url: url_path,
      success: function(result) {

     }
  })
 }
 

 function loadStatesBasedOnCountriesForUsers(id_value, list_name){
  url_path = "/admin_users/load_states_based_on_countries?"+list_name+"="+id_value;
  $.ajax({
      url: url_path,
      success: function(result) {

     }
  })
 }

 function getServicesOrVehiclesList(option){
  if (option==1) {
    $("#services_filter").hide();
    $("#services_list_id").val(0);
    $("#vehicles_filter").show();
  }else{
    $("#vehicles_filter").hide();
    $("#vehicles_list_id").val(0);
    $("#services_filter").show();
  }
 }

 function confirmSignOut() {
   var retVal = confirm("Are you sure?");
    if (retVal == true) { 
        window.location.href = "/users/sign_out";
        return true;
    } else {
        return false;
    }
}

function latLongVal(e,id){
  if(e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 9 || e.keyCode == 13 || (e.which == 118 && e.ctrlKey)){
    return true;
  }
  var length = (id == "lat_long_radius") ? 10 : 15
  var value = $("#"+id).val();
  var code = (e.keyCode ? e.keyCode : e.which);
  var reg = new RegExp('^[0-9.]+$');
  var cur_val = e.key;
  if(cur_val.match(reg) == null){
    alert("invalid character");
    return false;
  }
  if(code != 46){
    if(value.indexOf(".") == -1){
      if(value.length == 5){
        alert("please enter dot(.) here");
        return false;
      }
    }
    else{
         if(value.indexOf(".") != -1){
          left = value.split(".")[0].length
          right = value.split(".")[1].length
         var caretpos = document.getElementById(id).selectionStart;
                if(left == 5 && caretpos <= 5){
              alert("Only five character allow before dot(.)");
              return false; 
                     } 
                      }
                    }
    if(value.length == length){
      return false;
    }
  }else{
    value += "."
    if ((value.split('.').length-1) == 2){
      alert("you have given dot(.) already");
      return false;
  }
  }
}

function phoneNumberVal(e,id,phone_no_length){
  var value = $("#"+id).val();
  if(e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 9 || e.keyCode == 13){
    return true;
  }
  if(value.length == phone_no_length){
    return false;
  }
  var reg = new RegExp('^[0-9]+$');
  var cur_val = e.key;
  if(cur_val.match(reg) == null){
    return false;
  }
}

function showStatusAlert(status,device_status,is_paired){
  if (status == "mapped" ){
    alert("You can't delete the record with mapped status.");    
  }
  else if(status == "active"){
    if(is_paired == true){
    alert("Can't delete the record, device is active or vehicle is paired.");
    }
   else{
    alert("You can't delete the record with active status.");
   }
  }
  else if(status == "inactive" && is_paired == true){
      alert("Can't delete the record, device is inactive or vehicle is paired.");
    }
  else{
    if (status == "pending" && device_status == "true"){
      alert("you can't delete record since it's using by active users!")
    }
  } 
}

function subdomainVal(e,id){
  var value = $("#"+id).val();
  var reg = new RegExp('^[a-zA-Z0-9.]+$');
  var code = (e.keyCode ? e.keyCode : e.which);
  var cur_val = e.key;
  if(cur_val.match(reg) == null){
    return false;
  }
if(code == 46){
  value += "."
  if ((value.split('.').length-1) == 2){
      alert("you have given dot(.) already");
      return false;
  }
}
}

function getDuration(controller_name, action_name, dep_id,duration_id){
   var dep_time = $("#"+dep_id).val();
   var duration = $("#"+duration_id).val();
   if (dep_time == "" && duration != ""){
    alert("Please select departure time");
    return false;
   }
    url_path = "/"+ controller_name +"/" + action_name + "?dep_time=" +dep_time + "&duration=" + duration
    $.ajax({
      url : url_path,
      success : function(result) {

     }
  })
}


function onlyNumbers(evt){
    var e = evt
    if (window.event) { // IE
        var charCode = e.keyCode;
    }
    else 
        if (e.which) { // Safari 4, Firefox 3.0.4
            var charCode = e.which;
        }
    if (charCode == 46 || charCode == 9) 
        return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) 
        return false;
    return true;
}

function smsNumbersValidation(evt){
  var e = evt
    if (window.event) { // IE
        var charCode = e.keyCode;
    }
    else 
        if (e.which) { // Safari 4, Firefox 3.0.4
            var charCode = e.which;
        }
    if ($("#sms_mobile_numbers").attr("id") == "sms_mobile_numbers"){
      allow_char = 44
      if(charCode == allow_char){
        return true;
      }
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) 
        return false;
      return true;
}
function getLiveUpdate() {
    url_path = "/dashboard/get_live_update/"
    $.ajax({
      url : url_path,
      success : function(result) {

     }
  })
}

function gpsStatusFilter(e,is_form,status,vehicle_type){
  var gps_val = e.value
  var location = window.location
  var path = (is_form == "device_location") ? "/device_location?" : "/asset_holders?";
  path = (location.search.split("&")[0] == "?deleted=true") ? path+"deleted=true&" : path;
  if(status == 'asset_filter'){
    //window.location.replace(path+"gps_val="+gps_val+"");
    path = path+"gps_val="+gps_val+"&vehicles=" + vehicle_type
  }else{
    //window.location.replace(path+"running_status="+gps_val+"");
    path = path+"running_status="+gps_val+"&vehicles=" + vehicle_type
  }
  $.ajax({
    url: path,
    dataType: 'script',
    type: 'GET',
    success: function(data) {      
    },
    error: function(e) {}
  })
}

function feedbackForFilter(e) {
  var feedback_for = e.value
  var location = window.location
  var path = "/accounts/feedback_index?";
  window.location.replace(path+"feedback_for="+feedback_for+"");
}

function showAuditForModelName(model,date){
  var model = model.val();
  var date = date.val();
  url_path = "/audits/show_audit_for_model?model="+model+"&date="+date+"&is_return=true";
  // url_path = "/audits/show_audit_for_model?is_return=true&"+"model="+model.val();+"&date="+date_val;
	$.ajax({
		url: url_path,
		dataType: "script",
    });
}
  $( document ).ready(function() {
   $(".alert").fadeOut(5000);
  });

  function validatePincode(){
    var pin_code = $("#user_pin_code").val();
    if(pin_code.length != 6){
      alert("pincode must have 6 digits!")
      return false;
    }
  }

  function assetsStatusFilter(e){
    var running_status = e.value
    var location = window.location
    var path = "/asset_holders?";
    path = (location.search.split("&")[0] == "?deleted=true") ? path+"deleted=true&" : path;
    window.location.replace(path+"running_status="+running_status+"");
}

function handle_selected_report(){
  var subdomain = $('#report_id')[0].dataset['subdomain']
  if(subdomain == "sys"){
    var OPERATOR_ACCOUNT_USAGE_REPORT = 1;
    var PRODUCT_USAGE_REPORT = 2;
    var DEVICE_ORDER_REPORT = 3;
    if($("#report_id").val() == DEVICE_ORDER_REPORT.toString()){
      if($("#report_date_range option[value='6']").length > 0 ){
      //&& $("#report_date_range option[value='6']") === "undefined"
      //$("#report_date_range select").val("1");
        $("select[id=report_date_range] option:last").remove();      
        $('#report_date_range option:eq(1)').prop('selected', true)
        $("#report_filter_custom_date").hide();
      }
    }
  }
  else
  {
    var SHOW_NON_WORKING_DEVICE = 1;
    var SMS_TRACKERS_REPORT = 2;
    var SERVICES_TRACKING_REPORT = 3;
    var FEEDBACK_REPORT = 4;
    var TRACKING_LINK_ACCESS_REPORT = 5;
    var BUS_HALT_REPORT = 6;
    var DASHBOARD_TRACK_DATA = 7;
    var SPEED_LIMIT_REPORT = 8;
    var DEVICE_STATUS_REPORT = 9;
    var SERVICE_PERFORMANCE_REPORT = 10;
    var DEPARTURE_ARRIVAL_REPORT =  11;
    var STAGE_WISE_DEPARTURE_ARRIVAL_REPORT =  12;
    var STOPPAGE_VIOLATION_REPORT =  13;
    var WEEKLY_DELAY_REPORT = 14;
    var AVERAGE_ARRIVAL_DEPARTURE_REPORT = 15;
    var SERVICE_PERFORMANCE_REPORT_TYPE_2 =  16;
    var CONSOLIDATED_SERVICE_STATUS_REPORT =  17;
    var PANIC_REPORT =  18;
    var UNAUTH_VEHICLE_REPORT = 19;
    var PICKUP_SERVICE_TRACKING_REPORT = 20;
    var  UNAUTH_VEHICLE_SPEED_ANALYSIS_REPORT = 21;
  if ($("#report_id").val() != PANIC_REPORT.toString()){
      $("#report_from_date").datepicker("destroy");
      $("#report_to_date").datepicker("destroy");
      $('#report_from_date, #report_to_date').datepicker(
       { 
          endDate: new Date(),      
          autoclose: true
        });
    }
    if($("#report_id").val() != BUS_HALT_REPORT.toString() && $("#report_id").val() != STOPPAGE_VIOLATION_REPORT.toString() ){
    if($("#report_date_range option[value='6']").length > 0 ){
      //&& $("#report_date_range option[value='6']") === "undefined"
      //$("#report_date_range select").val("1");
      $("select[id=report_date_range] option:last").remove();      
      $('#report_date_range option:eq(1)').prop('selected', true)
      $("#report_filter_custom_date").hide();
    }
    }
    else
    {
      if($("select[id=report_date_range] option:last")[0].value != "6"){
        $("select[id=report_date_range] option:last").after("<option value='6'>Custom Time</option>");
      }
    }   
  }    
  report_type = $('#report_id').val() * 1.0;
  help_info_message = "";
  $("#btn_text_span").html("Run Report");
  $("#report_stage_type_div").hide();
  $("#report_date_range_div").hide();
  $("#services_track_date_div").hide();
  $(".cls_services_track_time_div").hide();
  $("#feedback_for_div").hide();
  $("#export_btn_div").hide();
  $("#run_report_div").show();
  $("#services_list_div").hide();
  $("#hubs_list_div").hide();
  $("#report_operators_div").hide();
  $("#report_dates_div").hide();
  $("#div_stage_type_halt").hide();  
  $("#dashboard_track_div").hide();  
  $("#vehicle_number_div").hide();
  $("#service_or_vehicle_id_div").hide();
  $("#place_name_div").hide();
  $("#report_vendors_div").hide();    
  $(".txt_time").attr('required',false);
  $("#report_place_id").attr('required',false);
  if ($("#report_service_id option[value='all']").length == 1){
    $("#report_service_id").find("option").eq(0).remove();  
  }
  if (report_type == SMS_TRACKERS_REPORT){
    if ($("#report_service_id option[value='all']").length == 0){
      $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
    }
    help_info_message = "Here, you can see sms trackers report."
    $("#report_date_range_div").show();
    $("#services_list_div").show();
    $("#hubs_list_div").show();
  }
  else if(report_type == SERVICES_TRACKING_REPORT){
    help_info_message = "Shows service trip details for selected service by travel date."
    $("#report_stage_type_div").show();
    $("#services_track_date_div").show();
    $("#btn_text_span").html("Export to CSV")
    $("#run_report_div").hide();
    $("#export_btn_div").show();
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    $("#link_stage_type_halt").attr("onclick","servicesTrackReportClick(); return false;");
    if ($("#report_service_id option[value='all']").length == 0){
    $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
  }
  }
  else if(report_type == FEEDBACK_REPORT){
    help_info_message = "Here you can see feedback report."
    $("#feedback_for_div").show();
    $("#report_date_range_div").show();
    $("#services_list_div").hide();
    $("#hubs_list_div").hide();
  }
  else if(report_type == TRACKING_LINK_ACCESS_REPORT){
    help_info_message = "Here you can see tracking link access report."
    $("#report_date_range_div").show();
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    if ($("#report_service_id option[value='all']").length == 0){
    $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
  }
  }
  else if(report_type == PRODUCT_USAGE_REPORT){
    help_info_message = "Here you can see product usage report."
    $("#services_list_div").hide();
    $("#hubs_list_div").hide();
    $("#report_operators_div").hide();
    $("#report_date_range_div").hide();
    $("#report_dates_div").show();
  }
  else if(report_type == OPERATOR_ACCOUNT_USAGE_REPORT){
    help_info_message = "Here you can see operator account usage report."
    $("#report_date_range_div").show();
    $("#services_list_div").hide();
    $("#hubs_list_div").hide();
    $("#report_operators_div").show();
    $("#report_date_range_div").hide();
    $("#report_vendors_div").hide();
    $("#report_operator_id option[value='all']").remove();    
  }
  else if(report_type == BUS_HALT_REPORT){
    $("#report_service_id option[value='all']").remove();
    help_info_message = "Here you can see bus halt report."
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    $("#service_or_vehicle_id_div").show()
    $('input[name="report[service_or_vehicle_id]"]:radio')[0].checked = true
    $("#report_date_range_div").show();
    if($("#report_date_range").val() == "6"){
      $(".txt_time").attr('required','required');
      $(".cls_services_track_time_div").show();      
    }
    else{
      $(".cls_services_track_time_div").hide();
      $(".txt_time").attr('required',false); 
    }
    //$("#link_stage_type_halt").attr("onclick","servicesTrackReportClick('halt'); return false;");
  }
   else if(report_type == STOPPAGE_VIOLATION_REPORT){
    $("#report_service_id option[value='all']").remove();
    help_info_message = "Here you can see stoppage violation report."
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    $("#service_or_vehicle_id_div").show()
    $('input[name="report[service_or_vehicle_id]"]:radio')[0].checked = true
    $("#report_date_range_div").show();
    if($("#report_date_range").val() == "6"){
      $(".txt_time").attr('required','required');
      $(".cls_services_track_time_div").show();      
    }
    else{
      $(".cls_services_track_time_div").hide();
      $(".txt_time").attr('required',false); 
    }
    //$("#link_stage_type_halt").attr("onclick","servicesTrackReportClick('halt'); return false;");
  }
  else if(report_type == SERVICE_PERFORMANCE_REPORT || report_type == SERVICE_PERFORMANCE_REPORT_TYPE_2){
    help_info_message = "Here you can see services performance report."
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    $("#report_date_range_div").show();
   }
   else if(report_type == DEPARTURE_ARRIVAL_REPORT || report_type == AVERAGE_ARRIVAL_DEPARTURE_REPORT){
    help_info_message = "Here you can see departure/arrival report."
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    $("#report_date_range_div").show();
   }
   else if(report_type == STAGE_WISE_DEPARTURE_ARRIVAL_REPORT){
    help_info_message = "Here you can see stage wise departure/arrival report."
    $("#place_name_div").show();
    $("#report_date_range_div").show();
    $("#report_place_id").attr('required','required');
   }
  else if(report_type == DASHBOARD_TRACK_DATA){
    help_info_message = "Here you can see dashboard track data report."
    $("#dashboard_track_div").show();
  }
  else if(report_type == SPEED_LIMIT_REPORT){
    help_info_message = "Here you can see speed limit report."
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    $("#service_or_vehicle_id_div").show()
    $('input[name="report[service_or_vehicle_id]"]:radio')[0].checked = true
    $("#services_track_date_div").show();
    if ($("#report_service_id option[value='all']").length == 0){
      $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
    }
    $("#report_vehicle_id option").eq(1).before($("<option></option>").val("all").text("All"));
  }
  else if(report_type == DEVICE_STATUS_REPORT){
    help_info_message = "Here you can see device status report."
    $("#vehicle_number_div").show();
    $("#services_track_date_div").show();
    $("#report_vehicle_id option[value='all']").remove();
  }
  else if(report_type == DEVICE_ORDER_REPORT){
    help_info_message = "Here you can see device order report."
    $("#report_operators_div").show(); 
    if ($("#report_operator_id option[value='all']").length == 0){
      $("#report_operator_id").prepend("<option value='all' selected='selected'>All</option>")
    }
    $("#report_date_range_div").show();
    $("#report_vendors_div").show();
    if ($("#report_vendor_id option[value='all']").length == 0){
      $("#report_vendor_id").prepend("<option value='all' selected='selected'>All</option>")
    }
    }
    else if (report_type == CONSOLIDATED_SERVICE_STATUS_REPORT){
      help_info_message = "Here you can see consolidated service status report."
      if ($("#report_vehicle_id").val() == ""){
    $("#services_list_div").show();
    $("#hubs_list_div").show();
  }
    if($("#report_service_id").val() == 1){
     $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
    }
    $("#hubs_list_div").show();
    $("#service_or_vehicle_id_div").show()
    if(($("#report_vehicle_id").val() != "")){
    $("#vehicle_number_div").show()
  }
    $("#report_date_range_div").show();
    if ($("#report_service_id").val() == "all"){
     if ($("#report_service_id option[value='all']").length == 0){
      $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
  }
}  if ($("#report_vehicle_id").val() == ""){
    $('input[name="report[service_or_vehicle_id]"]:radio')[0].checked = true
  }
}
else if(report_type == PANIC_REPORT){
  help_info_message = "Here you can see panic report."
    $("#report_date_range_div").show();
  if ($("input[name='report[service_or_vehicle_id]']:checked").val() == "service"){
    $("#services_list_div").show(); 
  }
  else{
    $("input[name='report[service_or_vehicle_id]'][value='vehicle']").prop('checked', true);
    $('#vehicle_number_div').show();
  }
    $('input[name="report[service_or_vehicle_id]"]:radio')[0].checked = true
    $("#service_or_vehicle_id_div").show();
    $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
    if ($("#report_vehicle_id option[value='all']").length == 0){
      $("#report_vehicle_id option").eq(1).before($("<option></option>").val("all").text("All"));
    }
    $("#report_from_date").datepicker("destroy");
    $("#report_to_date").datepicker("destroy");
    $('#report_from_date, #report_to_date').datepicker(
    { 
      startDate: '-15d',         
      endDate: new Date(),      
      autoclose: true
    });
  }
  else if(report_type == SHOW_NON_WORKING_DEVICE){
    help_info_message = "Here you can see non-working device."
    $("#report_date_range_div").show();
      
  }
  else if(report_type == UNAUTH_VEHICLE_REPORT){
    help_info_message = "Here you can see unauth vehicle report"
     $("#vehicle_number_div").show(); 
     $("#report_date_range_div").show();
  }
  else if(report_type == UNAUTH_VEHICLE_SPEED_ANALYSIS_REPORT){
    help_info_message = "Here you can see unauth vehicle speed analysis report"
     $("#vehicle_number_div").show();
    $("#services_track_date_div").show();
    $("#report_vehicle_id option[value='all']").remove();
  }
  else if(report_type == PICKUP_SERVICE_TRACKING_REPORT){
    help_info_message = "Here you can see pickup service tracking report"
    $("#report_stage_type_div").show();
    $("#services_track_date_div").show();
    $("#btn_text_span").html("Export to CSV")
    $("#run_report_div").hide();
    $("#export_btn_div").show();
    $("#services_list_div").show();
    $("#hubs_list_div").show();
    $("#link_stage_type_halt").attr("onclick","servicesTrackReportClick(); return false;");
    if ($("#report_service_id option[value='all']").length == 0){
    $("#report_service_id").prepend("<option value='all' selected='selected'>All</option>")
  }
  }
  if(help_info_message != ""){
    $("#report_help_info").html(help_info_message);
    $("#report_help_info").show();
  }
}

function loadHubServices(e){
  var id = e.value
   $.ajax({
    url: '/hubs/get_hub_services?id='+id+'',
    format: 'script',
    type: 'GET',
    success: function(data) {
      if(data.length > 0){
        $("#report_service_id").empty(); 
        var is_report = $.inArray($("#report_id").val(),['5','9','10']);
        if (is_report == -1)
        {
         $("#report_service_id").append($('<option></option>').val("all").html("All"));
        }
        $.each(data, function( index, val ) { 
          $("#report_service_id").append(
              $('<option></option>').val(val[1]).html(val[0])
          );
        });       
      }
      console.log(data);
    },
    error: function(e) {}
  })
}

function show_service_vehicle(ele){
  if(ele.value == "service"){
    $("#vehicle_number_div").hide();
    $("#services_list_div").show();
    $("#hubs_list_div").show();
  }
  else
  {
    $("#vehicle_number_div").show();
    $("#services_list_div").hide();
    $("#hubs_list_div").hide();
  }  
}

function servicesTrackReportClick(stage_type){
  var report_id_val = $("#report_id").val();
  var service_id_val = $("#report_service_id").val();
  var service_name_num = $("#report_service_id option:selected").text();  
  var stage_type_val = $("#report_stage_type").val();
  var hub_id = $("#report_hub_id").val();
  if (stage_type != null){
    var excel_name = "Bus Halt Report.xls"
  }
  else
  {     
    var excel_name = "Services Tracking Excel.xls"
  }
    var stage_type_name = $("#report_stage_type option:selected").text();
  var date_val = $("#report_selected_date").val();
  var url_path = "reports/services_tracking_xls?report_id="+report_id_val+"&service_id="+service_id_val+"&stage_type="+stage_type_val+"&date="+date_val+"&hub_id="+hub_id;
  $.ajax({
    url: url_path,
    success: function(data) {
      if (data != undefined) {
        var blob=new Blob([data]);
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(blob);
        var service_name_condition = "";
        if (service_name_num == "*"){
          service_name_condition = "Services";
        }else{
          service_name_condition = service_name_num;
        }
        if (stage_type_name == "--ALL--"){
          excel_name = service_name_condition+" on "+date_val+".xls";
        }else{
          excel_name = service_name_condition+" "+stage_type_name+" Details on "+date_val+".xls";
        }
        link.download=excel_name;
        link.click();
        if ($("#report_error_div") != undefined) {
          $("#report_error_div").html('');
          $("#report_error_div").hide();
        }
      }
      else{
        $("#report_error_div").html("There is no report for this data");
        $("#report_error_div").show();
        $("#report_results").html("");
      }
    }
  });
}

function validateAddMobileNumbers(){
  var mobile_numbers = $("#sms_mobile_numbers").val();
  data = true
  if (mobile_numbers.length < 10 && mobile_numbers.length != 0){
    data = false;
  }
  else if (mobile_numbers.length > 10){
    mobile_numbers_arr = mobile_numbers.split(',');
    $.each(mobile_numbers_arr, function(key, value){
      if(value.length != 10 ){
        data = false;
      }
    });
  }
  if(data == false){
    alert("Every number should have 10 character and seperated by comma(,)");
    return false;
  }
}

function trackingAccessReport(service_id, pnr_number,date){
 url_path = "reports/tracking_link_access_by?service_id=" + service_id + "&pnr_number=" + pnr_number + "&date=" + date
    $.ajax({
      url : url_path,
      success : function(result) {

     }
  })
}

  function getInvoice(account_id){
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    if(from_date == "" || to_date == ""){
      alert("Please select From date and To date");
      return false;
    }
      url_path = "/accounts/get_invoice?from_date="+from_date+"&to_date="+to_date + "&account_id=" + account_id;
      $.ajax({
          url: url_path,
          success: function(result) {

         }
      })
   }

   function Generate_Invoice(account_id, invoice_id, net_amount, is_from){
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    if(net_amount == '0.0'){
      alert("Net Amount should be graeater than Zero!");
      return false;
    }
    url_path = "/accounts/generate_invoice?from_date="+from_date+"&to_date="+to_date + "&account_id=" + account_id + "&id=" + invoice_id + "&is_from=" + is_from;
      $.ajax({
          url: url_path,
          success: function(result) {

         }
      })
   }

   function sendInvoiceEmail(account_id,from_date,to_date,invoice_id){
    url_path = "/accounts/send_invoice_mail?from_date="+from_date+"&to_date="+to_date + "&account_id=" + account_id  + "&id=" + invoice_id;
      $.ajax({
          url: url_path,
          success: function(result) {

         }
      })
   } 
    function getAllIds(class_name) {
    temp = $('.' + class_name);
    ids = [];
       for ( i = 0; i < temp.length; i++) {
        if (temp[i].checked == true) {
          ids.push(parseInt(temp[i]["value"]));
      }
    }
    return ids;
  }

// var checkboxClick = function(){
//   $(".cancal_service").click(function(){
//     var canc_ser_ids = getAllIds('cancal_service');
//     var checked_id = getCheckedId('cancal_service');
//      localStorage.setItem('canc_ser_ids',canc_ser_ids);
//      url_path = "/service_trips/cancel_service_ids?canc_ser_id="+checked_id;
//       $.ajax({
//           url: url_path,
//           success: function(result) {

//          }
//       })
//   })
// }

  function getCheckedId(class_name) {
    temp = $('.' + class_name);
    var id = parseInt(temp[0]["value"]);
    return id;
  }
  var checkboxClick = function(){

  $(".cancal_service").click(function(){
    var canc_ser_ids = getAllIds('cancal_service');
    var checked_id = getCheckedId('cancal_service');
     localStorage.setItem('canc_ser_ids',canc_ser_ids);
    if ($(this).is(':checked')) {
      var message = "Are you sure, Do you want to change the running status?"
    }else{
      var message = "Are you sure, Do you want to cancel the trip?"
    }
            var retVal = confirm(message);
      if (retVal == true) { 
         url_path = "/service_trips/cancel_service_ids?canc_ser_id="+checked_id;
          $.ajax({
          url: url_path,
          success: function(result) {
         }
      })
        return true;
    } else {
        return false;
    }
  })
}

function switchAccount(event_obj){
  selected_account_id = event_obj.value;
  if (selected_account_id != undefined) {
    url_path = "/accounts/switch_account?selected_account_id="+selected_account_id;
    $.ajax({
      url: url_path,
      success: function(result){
        if(result != undefined){
          window.open(result.redirect_to_url, '_blank');
        }
      }
    });
  }
}


function placeLatLongMapping(place_id){
  var lat_long = $("#lat_long_id").val();
    var stage_lat_long = lat_long.split(",")
    var stage_latitude = parseFloat(stage_lat_long[0])
    var stage_longitude = parseFloat(stage_lat_long[1])
    if(stage_latitude > 0 && stage_latitude > 0){
    var radius = $("#update_stage_radius").val();
    url_path = "/places/update_place_lat_long_mapping?lat_long="+lat_long + "&radius=" +radius + "&place_id="+place_id;
    $.ajax({
      url: url_path,
      success: function(result){
       window.location.reload();
      }
    });
  }
    else{
      alert("latitude and longitude are not correct");
    }
}

function resendSmsTracker(id,mobile_number,message){
  var confirm_val = confirm("Are you sure?, do you want to resend the sms?");
  if(confirm_val == true){
   url_path = "reports/resend_sms_tracker?id=" + id + "&mobile_number=" + mobile_number + "&message=" + message
    $.ajax({
      url : url_path,
      success : function(result) {
      alert("SMS resend successfully!");
      $(".modal-backdrop").attr("class","");

     }
  })
  }else{
    $(".modal-backdrop").modal('hide'); 
  }
}
   
   function checkRunningStatus(st_id) {
    if ($("#service_cancelled_"+st_id).prop('checked')){
      var message = "Are you sure, Do you want to change the running status?"
    }else{
      var message = "Are you sure, Do you want to cancel the trip?"
    }
      var retVal = confirm(message);
      if (retVal == true) { 
         url_path = "/service_trips/cancel_service_ids?canc_ser_id="+st_id;
          $.ajax({
          url: url_path,
          success: function(result) {
         }
      })
        return true;
    } else {
        return false;
    }
   }

   function getAccountInvoice() {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    var account_id = $("#account_id").val();
    if (from_date == ""){
      alert("please select from date")
      return false;
    }
    if (to_date == "") {
      alert("please select to date")
      return false;
    }
     url_path = "/accounts/account_invoice_filter?from_date="+from_date+"&to_date=" + to_date + "&account_id=" + account_id;
          $.ajax({
          url: url_path,
          success: function(result) {
         }
      })
   }
function loadVendors(role) {
  var role = $('#user_role_id').find(":selected").text();
 if(role == "Gps Vendor"){
    $("#vendor_id_div").show();
  }
  else{
    $("#vendor_id_div").hide();
    $("#user_vendor_id").val('');
  }
}

function getVendorInvoice(vendor_id) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    if(from_date == "" || to_date == ""){
      alert("Please select From date and To date");
      return false;
    }
      url_path = "/vendors/get_vendor_invoice?from_date="+from_date+"&to_date="+to_date + "&vendor_id=" + vendor_id;
      $.ajax({
          url: url_path,
          success: function(result) {

         }
      })
}

function GenerateVendorInvoice(vendor_id, invoice_id, net_amount, is_from) {
    var from_date = $("#from_date").val();
    var to_date = $("#to_date").val();
    if(net_amount == '0.0'){
      alert("Net Amount should be graeater than Zero!");
      return false;
    }
    url_path = "/vendors/generate_vendor_invoice?from_date="+from_date+"&to_date="+to_date + "&vendor_id=" + vendor_id + "&id=" + invoice_id + "&is_from=" + is_from;
      $.ajax({
          url: url_path,
          success: function(result) {

         }
      })
}
function generate_message(mobile_number_info,email_info) {
  var message = $("#message").val();
  url_path = "/device_orders/send_device_order_notifications?message="+message + "&mobile_number_info=" + mobile_number_info + "&email_info=" + email_info;
$.ajax({
     type: "GET",
     url: url_path,
     success: function (result) {
     }
});
}

function GetVendorAccountInfo(id, is_for){
  url_path = "/device_orders/get_vendor_details?id="+id + "&is_for=" +is_for;
    $.ajax({
     type: "GET",
     url: url_path,
     dataType: "JSON",
     success: function (result) {
      if(is_for == "vendor"){
      var email = result["email"];
      $("#vendor_email_id").val(email);
      }
      else{
        var email = result["email"];
        var contact = result["contact"];
        var address = result["address"];
        $("#account_email_id").val(email);
        $("#account_contact_number").val(contact);
        $("#account_address_id").val(address);
      }  
     }
});
}

function serviceOfPlaceFilter(service_id,deleted) {
  url_path = "/places/place_service_filter?service_id="+service_id + "&deleted=" +deleted;
    $.ajax({
         type: "GET",
         url: url_path,
         success: function (result) {
         }
    });
}

function GetRegionEmailIds(region_id) {
  var region_ids = $("#device_order_region").val();
    url_path = "/device_orders/get_region_mail_ids?id="+region_id +"&region_id=" + region_ids;
    $.ajax({
     type: "GET",
     url: url_path,
     dataType: "JSON",
     success: function (result) {
        var region_emails = result["email_ids"];
        var cc_email_ids = result["cc_email_ids"];
        if(cc_email_ids == ""){
        $("#region_wise_cc_email_ids").val(region_emails);
        }else{
          $("#region_wise_cc_email_ids").val(region_emails +","+ cc_email_ids);
        }
     }
});
}

function printDivArea(divName) {
  var printContents = document.getElementById(divName).innerHTML;     
  var originalContents = document.body.innerHTML;     
  document.body.innerHTML = printContents;    
  window.print();     
  document.body.innerHTML = originalContents;
}

function hidePopup() {
  $("#track_popup").hide();
  $(".modal-backdrop").hide();
}
function hideInvoice() {
  $("#popup_div").hide();
  $(".modal-backdrop").hide();
}

function filterDevices(){
  account_id = $("#account_id").val();
  vendor_id = $("#vendor_id").val();
    url_path = "/devices/filter_account_device?vendor_id="+vendor_id + "&account_id=" + account_id
    $.ajax({
      url: url_path,
      success: function(result){
      }
    });
}

function filterVendorDevice(deleted,account_id){
  vendor_id = $("#device_vendor_id").val();
  if(account_id == undefined){
    url_path = "/devices?vendor_id="+vendor_id + "&status=unassigned" + "&deleted=" + deleted
  }
  else
  {    
    url_path = "/devices?vendor_id="+vendor_id + "&account_id=" + account_id + "&deleted=" + deleted
  }
  $.ajax({
    type: "get",
    url: url_path ,
    dataType: "script",   
    success: function(json){
      //alert('hi!');         
    },
    error: function() {
      alert('Something went wrong!');
   }
  });
}
function filterNonPerformDevice(){
  vendor_id = $("#non_perform_vendor_id").val();
  url_path = "/dashboard/sys_index_analytics?vendor_id="+vendor_id + "&data=non_perform_devices"
   $.ajax({
      type: "get",
      url: url_path,
      dataType: "script",  
      success: function(result){
      }
    });
}

// function get_address_from_google(lat_long,element,astl_id){
//     var lat_long = lat_long;
//     $.ajax({
//     url: "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat_long[0]+ "," + lat_long[1]+"&sensor=true",
//     dataType: 'JSON',
//     type: 'GET',
//     async: true,
//     success: function(data) {
//       element.text(data["results"][0]["formatted_address"]);
//       save_location_in_astl(astl_id,data["results"][0]["formatted_address"])
//     },
//     error: function() {
     
//     }
//     })
//   }

  function save_location_in_astl(astl_id,address,class_name){
    var astl_id = astl_id;
    var class_name = class_name;
    $.ajax({
    url: "/services/"+astl_id+"/update_address_in_astl?class_name="+class_name+"&location=" + address + "&",
    dataType: 'JSON',
    type: 'GET',
    async: true,
    success: function(data) {
      
    },
    error: function() {     
    }
    })
  }


function addLocation(){
  $(".spn_device_location").each(function() {
    var lat_long = this.dataset["latlong"].split(",")
    var element = this
    var astl_id = this.dataset["astl"]
    if (lat_long.length == 2){
      geocodeService.reverse().latlng(lat_long).run(function(error, result) { 
        //if(result.address.Match_addr.length > 6){
          $(element).text(result.address.Match_addr)
          save_location_in_astl(astl_id,result.address.Match_addr,"AssetTrackerLatest") 
        //}
        // else
        // {
        //   // get_address_from_google(lat_long,$(element),astl_id)
        // }      
  })
  }
  })
  }


  function unmergeAssets(asset1,asset2,page){
    var r=confirm("Are you sure to delete the paired vehicle?")
    if (r==true)
    {
      //if (page == undefined){
        var url = "/eta/device_location_index?vehicles=paired&asset_id1="+asset1 + "&asset_id2="+asset2
      // }
      // else
      // {
      //   var url = "/eta/device_location_index?vehicles=paired&asset_id1="+asset1 + "&asset_id2="+asset2+ "&page=" + page
      // }
      window.location.replace(url);

      // $.ajax({
      //   url : url,
      //   dataType: 'script',
      //   type: 'GET',
      //   success: function(result) { 
      //    // window.location.reload();
      //    },
      //   error: function() {
      //   }
      //   }) 
    }        
  }

   function add_remove_paired_ids(ele){
      var val = ele.value
      if (ele.checked == true){
        pairing_vehicles_arr.push(val)
      }
      else
      {
        pairing_vehicles_arr.splice($.inArray(val, pairing_vehicles_arr),1);
      }
      $("#pairing_vehicles").val(pairing_vehicles_arr)
      $.each(pairing_vehicles_arr, function( index, value ) {
        $("#chk_checkbox_" + value).attr('checked','checked')
      })
      
    }

function gpsVehiclesFilter(e){
    var gps_val = e.value
    var location = window.location
    var path = "/asset_holders?";
    path = (location.search.split("&")[0] == "?deleted=true") ? path+"deleted=true&" : path;
    window.location.replace(path+"gps_val="+gps_val+"");
}

function loadVendorsBasedOnAccounts(value,account_id){
 if ($("#report_id").val() == "3"){
   url_path = "/reports/load_vendors_based_on_accounts?"+account_id+"="+value;
    $.ajax({
        url: url_path,
        success: function(result) {

       }
    })
  }
}